'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _inDOM = require('../util/inDOM');

var _inDOM2 = _interopRequireDefault(_inDOM);

var _querySelectorAll = require('./querySelectorAll');

var _querySelectorAll2 = _interopRequireDefault(_querySelectorAll);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var matches = void 0;
if (_inDOM2.default) {
  matches = function () {

    function ie8MatchesSelector(node, selector) {
      var matches = (0, _querySelectorAll2.default)(node.document || node.ownerDocument, selector),
          i = 0;

      while (matches[i] && matches[i] !== node) {
        i++;
      }return !!matches[i];
    }

    var fn = null;
    return function (node, selector) {
      if (fn === null) {
        var body = document.body;

        if (body) {
          var nativeMatch = body.matches || body.matchesSelector || body.webkitMatchesSelector || body.mozMatchesSelector || body.msMatchesSelector;

          fn = nativeMatch ? nativeMatch : ie8MatchesSelector;
        }
      }
      return fn.call(node, selector);
    };
  }();
}

exports.default = matches;
module.exports = exports['default'];